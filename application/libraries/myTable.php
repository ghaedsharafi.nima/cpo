<?php 
/**
* Table Generator For Ease Of User in Ajax Request!
* Designed For Laravel 3
* Require Header.php and Cell.php
*/
class MyTable
{
	protected $headers = [];
	protected $cells = array();
	protected $tableClass = 'table table-hover';
	protected $tableID = '';
	protected $headClass = '';
	protected $rowClass = '';
	protected $cellClass = '';
	protected $operationClass = '';
	protected $headCellClass = '';
	protected $headCellWidth = '';
	protected $dirUp = 'dir-up';
	protected $dirDown = 'dir-down';
	protected $route = '';
	protected $onEmpty = '';
	protected $data = null;
	protected $dir = 'desc';
	protected $orderby = '';
	protected $operation = '';
	protected $openTagOnEmpty = '<div class="error"><h4>';
	protected $closeTagOnEmpty = '</h4></div>';

	function __construct(array $initialize = array()) {
		$this->initialize($initialize);
	}
	
	private function initialize(array $initialize)
	{
		$classVar = get_class_vars(get_class($this));
		foreach ($initialize as $key => $value)
		{
			$this->$key = $value;
		}
	}

	private function tableOpenTag()
	{
		$string = '<table';
		
		if($this->tableClass != '')
			$string .= ' class="'. $this->tableClass . '" ';
		if($this->tableID != '')
			$string .= ' id="'. $this->tableID . '" ';
		
		$string .= '>';
		
		return $string;
	}

	private function tableCloseTag()
	{
		return '</table>';
	}

	private function headOpenTag()
	{
		$string = '<thead ';
		
		if($this->headClass != '')
			$string .= 'class="'. $this->headClass . '"';
		
		$string .= ' >';

		return $string;
	}
	private function headCell($header, $key)
	{
		$string = '<td';
		
		if($header == 'operation' and $this->operationClass != '')
			$string .= ' class="'. $this->operationClass . '" ';
		else if($this->headCellClass != '')
			$string .= ' class="'. $this->headCellClass . '" ';

		if($this->headCellWidth != '')
			$string .= ' width="'. $this->headCellWidth . '" ';
		
		$string .= '>';
		if( $key->enabled )
		{
			$direction = '';
			if($this->orderby == $header)
			{
				if($this->dir == 'asc')
					$direction = 'desc';
				else
					$direction = 'asc';

			}
			$string .= '<a href="' . URL::to_route($this->route, array($header, $direction)). '"';
			if($this->orderby == $header)
			{
				if($this->dir == 'asc')	
					$string .= ' class= "' . $this->dirUp . '"';
				if($this->dir == 'desc')
					$string .= ' class= "' . $this->dirDown . '"';
			}
			$string .= '>';
			$string .= $key->title;
			$string .= '</a>';
		}
		else
		{
			$string .= $key->title;
		}
		$string .= '</td>';

		return $string;
	}
	private function rowOpenTag()
	{
		$string = '<tr ';
		if($this->rowClass != '')
			$string .= 'class="'. $this->rowClass . '"';
		$string .= ' >';

		return $string;
	}

	private function cellOpenTag()
	{
		$string = '<td ';
		if($this->cellClass != '')
			$string .= 'class="'. $this->cellClass . '"';
		$string .= ' >';

		return $string;
	}
	private function rowCloseTag()
	{
		return '</tr>';
	}
	private function cellCloseTag()
	{
		return '</td>';
	}
	private function headCloseTag()
	{
		return '</thead>';
	}
	private function makeHeaders()
	{
		$string = '';
	
		$string .= $this->headOpenTag();
		
		foreach($this->headers as $header => $key)
			$string .= $this->headCell($header, $key);
		
		$string .= $this->headCloseTag();

		return $string;
	}
	private function eachRow($row)
	{
		$string = '';

		foreach($this->cells as $cell)
		{
			$string .= $this->cellOpenTag();
			$string .= $this->makeCell($row, $cell);
			$string .= $this->cellCloseTag();
		}
		return $string;
	}	
	private function makeCell($row, $cell)
	{
		if($cell->name == 'operation')
		{	
			$param = [];
			foreach ($cell->param as $key => $value)
			{
				$value = '<?php return ' . $value . ' ?>';
				$string = eval("?> $value <?php ");
				$param[] = $string;
			}
			return vsprintf($this->operation, $param);
		}
		if($cell->func != '')
		{
			return call_user_func_array($cell->func, [$this->getExactValue($row, $cell->name), $cell->param]);
		}

		return $this->getExactValue($row, $cell->name);
	}
	private function getExactValue($row, $name)
	{
		$tempRow = clone $row;
		foreach( explode('->', $name) as $key => $value)
		{
			$rtrim = rtrim($value, '()');
			if( $rtrim != $value )
			{
				$temp = $tempRow->{$rtrim}();
			}
			else
			{
				$temp = $tempRow->{$value};
			}
			unset($tempRow);
			$tempRow = $temp;
		}
		return $tempRow;
	}
	public function makeRows()
	{
		$string = '';

		if(count($this->data) == 0)
			return $this->openTagOnEmpty . $this->onEmpty . $this->closeTagOnEmpty;
		
		foreach($this->data as $row)
		{
			$string .= $this->rowOpenTag();
			$string .= $this->eachRow($row);
			$string .= $this->rowCloseTag();
		}
		return $string;
	}
	public function render()
	{
		if(count($this->data) == 0)
			return $this->openTagOnEmpty . $this->onEmpty . $this->closeTagOnEmpty;
		return $this->tableOpenTag() . $this->makeHeaders() . $this->makeRows() . $this->tableCloseTag();
	}
}