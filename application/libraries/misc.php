<?php 

class Misc
{
	public static function makeCache($name, $data, $time)
	{
		Cache::put($name, $data, $time);
	}
	public static function incView()
	{
		Symfony\Component\HttpFoundation\Request::trustProxyData();
		$info = ['REQUEST_URI' => $_SERVER['REQUEST_URI'], 'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT'], 'USER_IP' => Request::ip() ];
		$data = [
			'ip' => Request::ip(),
			'server' => serialize($info),
		];
		Viewlog::create($data);
	}

	public static function upload($keyword, $directory, $name = null)
	{
		$urls = [];
		if(!file_exists($directory))
			mkdir($directory);

        $i = 0;
        $names = Input::file($keyword);
        foreach(Request::foundation()->files->get($keyword) as $file) 
        {
            if(!is_null($file) && $file->getClientSize() <= $file->getMaxFilesize() ) 
            {
				$name = md5(time() . md5($names['name'][$i])) . '.' . File::extension($names['name'][$i]);
				$urls[$i] = $name;
				$file->move($directory, $name );
				$i++;
			}
        }
        return $urls;
	}

	public static function uploadaFile($keyword, $directory)
	{
		if(!file_exists($directory))
			mkdir($directory);
        
        $inputFile = Input::file($keyword);

		$md5 = md5(time() . $inputFile['name']);
		$name = $md5 . '.' . File::extension($inputFile['name']);

        Input::upload($keyword, $directory, $name);
        
        return $directory . $name;
	}

	public static function matchLink($url1, $url2)
	{
		 return preg_match("/^" . Misc::htmlent($url1) .  '(.)*$/', $url2);
	}
	public static function make_badge($count)
	{
		$string = '';
		if($count != 0)
		{
			$string = '<span class="badge badge-warning" id="tickets">';
			$string .= $count;
			$string .= "</span>";
		}
		return $string;
	}
	public static function niceDate($format = 'j F y - H:i')
	{
		return jDate::forge()->format($format);
	}
	public static function niceDateForge($date, $format = 'j F y - H:i')
	{
		return jDate::forge($date)->format($format);
	}
	public static function niceDateReForge($date, $reforge, $format = 'j F y - H:i')
	{
		return jDate::forge($date)->reforge($reforge)->format($format);
	}
	public static function niceDateFromTimestamp($timestamp, $format = 'j F y - H:i')
	{
		$date = Date('Y-m-d H:i', $timestamp);
		return self::niceDateForge($date, $format);
	}
	public static function yearForge($date)
	{
		return Misc::niceDateForge($date, 'y');
	}
	public static function encrypt($string)
	{
		return base64_encode($string);
	}
	public static function decrypt($encrypted)
	{
		return base64_decode($encrypted);
	}
	public static function alert($msg, $state = 'error')
	{
		return '<div class="alert alert-'. $state . '"><a data-dismiss="alert" class="close">&#x2715;</a>' . $msg . '</div>';
	}
	public static function error($msg)
	{
		return '<span class="error" >' . $msg . '</span>';
	}

	public static function filter($route, $msg, $state = 'warning')
	{
		if(Request::ajax())
		{
			Session::flash('msg', $msg);
			Session::flash('state', $state);
			return "<script type='text/javascript'> window.location.href ='". URL::to_route($route) ."'; </script>";
		}
		return Redirect::to_route($route)->with('msg', $msg)->with('state', $state);
	}
	public static function makeSelect($array, $key, $value, $secValue = '', $seprator = ' ')
	{
		$data = [];
		foreach($array as $row)
		{
			$data[ $row->$key ] = $row->$value . (($secValue != '') ? $seprator . $row->$secValue : '');
		}
		return $data;
	}
	public static function encodeID($id)
	{
		return base_convert($id, 10, 35);
	}
	public static function decodeID($string)
	{
		return base_convert($string, 35, 10);
	}
	public static function cutWord($string, $num = '100')
	{
		$letnum = strlen(self::br2nl($string));

		if($letnum > $num)
		{
			$string = substr($string, 0, $num - 4) . '...';
		}
		return str_replace('<br />', ' ', $string);
	}
	public static function accessLabel($access)
	{
		if($access == ADMIN_ACCESS)
			return self::makeLabel('مدیر', 'success');
		return self::makeLabel('کاربر', 'success');

	}
	public static function makeLabel($string, $type)
	{
		$label = '';
		if(count($string) > 0)
		{
			$label = '<span class="label label-'. $type .'" >';
			$label .= $string;
			$label .= "</span>";
		}
		return $label;
	}
	public static function mPaginator($dbData, $per_page)
	{
		$total_rows = count($dbData);
		$total_pages = ceil($total_rows / $per_page);
        $page = Input::get('page', 1);

        if ($page > $total_pages or $page < 1)
        {
            $page = 1;
        }
        $offset = ($page * $per_page) - $per_page;
        $dbData = array_slice($dbData, $offset, $per_page);
        return Paginator::make($dbData, $total_rows,  $per_page);
	}
	public static function br2nl($string)
	{
		return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
	}	
	public static function formError($errors, $name)
	{
		if($errors->has($name))
		{
			return Misc::error( $errors->first($name) );
		}
		return ;
	}
	public static function trueFalseSign($value, $trueValue)
	{
		if($value == $trueValue)
			return '<p class="active-sign"><i class="icon-ok-sign icon-2x"> </i></p>';
		return '<p class="deactive-sign"><i class="icon-remove-sign icon-2x" > </i></p>';
	}
}