<?php

class Auth_Controller extends Base_Controller 
{
	public function get_login()
	{
		return View::make('login');
	}
	public function post_login()
	{
		if( trim(Input::get('username')) == '' || trim(Input::get('password')) == '' )
		{
			return Redirect::to_route('login')->with('msg', 'لطفاً اطلاعات را کامل وارد کنید.'); 
		}
		$input = Input::all();
		$creds = array(
			'username' => $input['username'],
			'password' => $input['password'],
			'remember' => isset($input['rememberme'])
		);

		if( Auth::attempt($creds) )
		{
			$msg = '';
			$route = 'home';
		}
		else
		{
			$msg = 'اطلاعات وارد شده صحیح نمی باشد.';
			$route = 'login';
		}

		return Redirect::to_route( $route )->with('msg', $msg);
	}
	public function get_logout()
	{
		Session::flush();
		Auth::logout();
		return Redirect::to_route('home');
	}
}