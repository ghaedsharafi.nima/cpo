<?php

class Reports_Controller extends Base_Controller 
{
	public function get_index()
    {
        $data['title'] = 'گزارش ها';
        $data['reports'] = Report::with('author')->order_by('id', 'desc')->paginate();

        return View::make('reports.index', $data);
    }    

	public function get_create()
    {
        $data['title'] = 'گزارش جدید';
        
        return View::make('reports.new', $data);
    }    

	public function post_create()
    {
        $input = Input::all();

        $data = [
            'subject' => $input['subject'],
            'content' => $input['content']
        ];

        Auth::User()->report()->insert($data);

        return Redirect::to_route('reportsindex')->with('msg', 'گزارش شما با موفقیت ثبت شد.')->with('state', 'info');
    }    

	public function get_update($id)
    {
        $data['title']  = 'ویرایش گزارش';
        $data['report'] = Report::find($id);

        return View::make('reports.edit', $data);
    }    

	public function post_update()
    {
        $input = Input::all();

        $report = Report::find($input['rid']);
        $report->subject = $input['subject'];
        $report->content = $input['content'];
        $report->save();

        return Redirect::to_route('reportsindex')->with('msg', 'گزارش شما با موفقیت ویرایش گردید.')->with('state', 'info');
    }    

	public function get_show($id)
    {
        $data['title'] = 'مشاهده گزارش';
        $data['report'] = Report::with('author')->find($id);

        return View::make('reports.show', $data);
    }    

	public function get_delete($id)
    {
        $mem = Report::find($id);
        $mem->delete();

        return Redirect::to_route('reportsindex')->with('msg', 'گزارش شما با موفقیت حذف گردید.')->with('state', 'info');
    }
}