<?php

class Article_Controller extends Base_Controller 
{
	public function get_index()
    {
        $data['title'] = 'مقاله ها';
        $data['articles'] = Article::with('author')->order_by('id', 'desc')->paginate();

        return View::make('articles.index', $data);
    }

	public function get_create()
    {
        $data['title'] = 'مقاله جدید';
        
        return View::make('articles.new', $data);
    }    

	public function post_create()
    {
        $input = Input::all();

        $data = [
            'subject' => $input['subject'],
            'content' => $input['content']
        ];

        Article::create($data);

        return Redirect::to_route('articlesindex')->with('msg', 'مقاله شما با موفقیت ثبت شد.')->with('state', 'info');
    }    

	public function get_update($id)
    {
        $data['title']  = 'ویرایش مقاله';
        $data['article'] = Article::find($id);

        return View::make('articles.edit', $data);
    }    

	public function post_update()
    {
        $input = Input::all();

        $report = Article::find($input['rid']);
        $report->subject = $input['subject'];
        $report->content = $input['content'];
        $report->save();

        return Redirect::to_route('articlesindex')->with('msg', 'مقاله شما با موفقیت ویرایش گردید.')->with('state', 'info');
    }    

	public function get_show($id)
    {
        $data['title'] = 'مشاهده مقاله';
        $data['article'] = Article::with('author')->find($id);

        return View::make('articles.show', $data);
    }    

	public function get_delete($id)
    {
        $article = Article::find($id);
        $article->delete();

        return Redirect::to_route('articlesindex')->with('msg', 'مقاله شما با موفقیت حذف گردید.')->with('state', 'info');
    }
}