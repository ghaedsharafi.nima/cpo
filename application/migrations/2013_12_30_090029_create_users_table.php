<?php

class Create_Users_Table {    

	public function up()
    {
		Schema::create('users', function($table) {
			$table->increments('id');
			$table->string('username');
			$table->string('password');
			$table->string('name');
			$table->integer('access');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('users');

    }

}