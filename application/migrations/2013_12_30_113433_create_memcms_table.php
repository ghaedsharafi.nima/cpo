<?php

class Create_Memcms_Table {    

	public function up()
    {
		Schema::create('memcms', function($table) {
			$table->increments('id');
			$table->integer('mid');
			$table->string('name');
			$table->text('comment');
			$table->integer('accepted');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('memcms');

    }

}