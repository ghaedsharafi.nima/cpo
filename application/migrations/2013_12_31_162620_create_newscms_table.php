<?php

class Create_Newscms_Table {    

	public function up()
    {
		Schema::create('newscms', function($table) {
			$table->increments('id');
			$table->integer('nid');
			$table->string('name');
			$table->text('comment');
			$table->integer('accepted');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('newscms');

    }

}