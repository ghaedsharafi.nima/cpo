<?php

class Create_Reports_Table {    

	public function up()
    {
		Schema::create('reports', function($table) {
			$table->increments('id');
			$table->string('subject');
			$table->text('content');
			$table->integer('uid');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('reports');

    }

}