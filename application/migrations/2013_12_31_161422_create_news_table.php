<?php

class Create_News_Table {    

	public function up()
    {
		Schema::create('news', function($table) {
			$table->increments('id');
			$table->string('subject');
			$table->text('content');
			$table->integer('uid');
			$table->timestamps();
	});

    }    

	public function down()
    {
		Schema::drop('news');

    }

}