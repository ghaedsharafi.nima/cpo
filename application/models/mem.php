<?php

class Mem extends Eloquent 
{
	public static $table = 'memories';

	public function author()
	{
		return $this->belongs_to('user', 'uid');
	}
	public function comments()
	{
		return $this->has_many('memcm', 'mid');
	}
	// Override
	public function delete()
	{
		$this->comments()->delete();
		return parent::delete();
	}
}