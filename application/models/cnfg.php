<?php

class Cnfg extends Eloquent 
{
	public static $table = 'configs';
	public static $timestamps = false;

	public static function getConfig($ckey)
	{
		return self::where('ckey', 'like', '%' . $ckey . '%')->first();
	}
}