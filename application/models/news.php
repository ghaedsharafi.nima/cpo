<?php

class News extends Eloquent 
{
	public static $table = 'news';

	public function author()
	{
		return $this->belongs_to('user', 'uid');
	}
	public function comments()
	{
		return $this->has_many('newscm', 'nid');
	}
}