@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')

{{ Form::open(URL::to_route('newarticle')) }}

    <h2> {{ $title }}  {{ Form::submit('ثبت', ['class' => 'btn btn-warning']) }}</h2>

	{{ Form::text('subject', '', ['class' => 'span4', 'placeholder' => 'عنوان مقاله']) }}
	<div>
		{{ Form::textarea('content', '', ['class' => 'span6 makeTextNice', 'placeholder' => 'متن مقاله ...']) }}
	</div>
	
{{ Form::close() }}

@endsection
