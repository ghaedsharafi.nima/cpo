@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2> {{ $article->subject }} </h2>

<div>
	<h4><small>ثبت در تاریخ {{ Misc::niceDateForge($article->created_at, 'j F y') }}</small></h4>
	<p>{{ $article->content }}</p>
</div>

@endsection
