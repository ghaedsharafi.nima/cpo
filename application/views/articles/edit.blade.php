@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')

{{ Form::open(URL::to_route('updatearticle')) }}
{{ Form::hidden('rid', $article->id) }}
    <h2> {{ $title }}  {{ Form::submit('ثبت', ['class' => 'btn btn-warning']) }}</h2>

	{{ Form::text('subject', $article->subject, ['class' => 'span4', 'placeholder' => 'عنوان گزارش']) }}
	<div>
		{{ Form::textarea('content', $article->content, ['class' => 'span6 makeTextNice', 'placeholder' => 'عنوان گزارش']) }}
	</div>
	
{{ Form::close() }}

@endsection
