@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2> {{ $report->subject }} </h2>

<div>
	<h4><small>توسط: {{ $report->author->name }} در تاریخ {{ Misc::niceDateForge($report->created_at, 'j F y') }}</small></h4>
	<p>{{ $report->content }}</p>
</div>

@endsection
