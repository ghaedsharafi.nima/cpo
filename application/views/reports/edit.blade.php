@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')

{{ Form::open(URL::to_route('updatereport')) }}
{{ Form::hidden('rid', $report->id) }}
    <h2> {{ $title }}  {{ Form::submit('ثبت', ['class' => 'btn btn-warning']) }}</h2>

	{{ Form::text('subject', $report->subject, ['class' => 'span4', 'placeholder' => 'عنوان گزارش']) }}
	<div>
		{{ Form::textarea('content', $report->content, ['class' => 'span6 makeTextNice', 'placeholder' => 'عنوان گزارش']) }}
	</div>
	
{{ Form::close() }}

@endsection
