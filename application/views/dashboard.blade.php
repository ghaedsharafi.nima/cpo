@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('style')
    
@endsection

@section('content')
    <h2> {{ $title }} </h2>
	<ul class="thumbnails">
	<?php
	foreach($articles->results as $article)
	{
		$hClass = 'span4';
		if($article->highlight)
			$hClass = 'span9'; 
	?>
	<div class="thumbnail">
	    <img src="{{ URL::base() . '/' . $article->thumb }}" alt="{{ $article->title }}">
	    <div class="caption">
	    	<h3>{{ $article->title }}</h3>
			<p>{{ $article->short_story }}</p>
			<p align="center"><a href="http://bootsnipp.com/" class="btn btn-primary btn-block">Open</a></p>
	    </div>
  	</div>
    <?php } ?>
    </ul>
@endsection


@section('script')
    <script type="text/javascript">
	    var $container = $('#container');
	    // initialize
	    $container.masonry({
	    	columnWidth: 200,
	    	itemSelector: '.item'
	    });
    </script>
@endsection