@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2> {{ $title }} </h2>
	<?php if(Session::has('msg')) { echo Misc::alert(Session::get('msg'), Session::get('state') ); } ?>

@foreach($memories->results as $memory)
<div>
	<div class="span8 well">
        <h2>
        	{{ $memory->subject }}
        	@if(Auth::User()->id == $memory->uid)
        		<a href="{{ URL::to_route('deletememory', $memory->id) }}" class="btn pull-left" onclick="return confirm('آیا از حذف این خاطره اطمینان دارید؟')"><i class="icon-trash"></i></a>
        		<a href="{{ URL::to_route('editmemory', $memory->id) }}" class="btn pull-left"><i class="icon-edit"></i></a>
        	@endif
        </h2>
        <h4><small>توسط: {{ $memory->author->name }} در تاریخ {{ Misc::niceDateForge($memory->created_at, 'j F y') }} <i class="icon-comments"></i> {{ $memory->comments()->count() }}
        <small></h4>
        <p>{{ Misc::cutWord($memory->mem, 230)}}</p>
        <h6><a class="pull-left btn" href="{{ URL::to_route('showmemory', $memory->id) }}">ادامه خاطره... </a></h6>
    </div>
</div>
@endforeach
<div class="pagination pagination-centered">
    {{ $memories->links() }}
</div>
@endsection
