<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title> {{ Cnfg::getConfig('portalname')->cvalue }} - @yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" type="image/png" href="{{ URL::base() . '/public/img/fav.png' }}">
	{{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::style('css/bootstrap-responsive.min.css') }}
	{{ HTML::style('css/font-awesome.min.css') }}
	{{ HTML::style('css/jquery.notific8.min.css') }}
	{{ HTML::style('css/style.css') }}
	@yield('style')
</head>
<body>
<?php $user = Auth::User(); ?>
	<div class="navbar navbar-fixed-top navbar-inverse">
	  <div class="navbar-inner">
		<div class="container-fluid">
		  <a class="brand" href="{{ URL::to_route('home') }}" style="font-family:'BYekan'" >{{ Cnfg::getConfig('portalname')->cvalue }}</a>
		   <ul class="nav">
		  </ul>
			<ul class="navbar-text pull-left settings">
			خوش آمدید 
			@if(!is_null($user))
			  {{ $user->name }}&nbsp;
			  <a href="{{ URL::to_route('logout')}}" class="navbar-link"><i class="icon-large icon-off"></i></a>
			@else
				مهمان, <a href="{{ URL::to_route('login')}}" class="navbar-link"><i class="icon-large icon-signin"></i></a>
			@endif
			</ul>
		</div>
	  </div>
	</div>

	<div class="container-fluid">
	  <div class="row-fluid">
	  	@include('menu') 	
		<div id="cont" class="span9">
			@yield('content')
		</div><!--/span-->
	  </div><!--/row-->

	<hr>
	<footer>
		<p>طراحی و اجرا توسط <a href="http://code-nevis.ir" target="_blank" >کد نویس</a> 1392 - {{ jDate::forge()->reforge('+ 1 year')->format('Y'); }} - نسخه 0.1</p>
		</p>
	</footer>
	</div><!--/.fluid-container-->
	<div id="ajaxLoading" style="display:none">
		<p>
		{{ HTML::image('img/indicator-big.gif', 'لطفاً منتظر بمانید.'); }}
		<h4>لطفاً کمی تأمل فرمائید.</h4>
		</p>

	</div>

	{{ HTML::script('js/jquery.js'); }}
	{{ HTML::script('js/jquery.ui.js'); }}
	{{ HTML::script('js/bootstrap.min.js'); }}
	{{ HTML::script('js/bootbox.min.js'); }}
	{{ HTML::script('js/ajax.js') }}
	
	<script type="text/javascript">

		$('#menu *').disableSelection();
		$('#menu.nav li').mouseover(function(){
			$(this).find('a i').addClass('icon-2x');
			$(this).find('a').css('font-weight', 'bold');
		}).on("mouseleave",function(){
			$(this).find('a i').delay(200).removeClass('icon-2x');
			$(this).find('a').delay(200).css('font-weight', 'normal');
		});
		$('.nav-header').click(function() {
			$(this).nextUntil('li.divider').toggle('hide');
			var item = $(this).find('i');
			if(item.attr('class') == 'icon-chevron-left') {
				item.attr('class' , 'icon-chevron-down');
			}
			else {
				item.attr('class' , 'icon-chevron-left');
			}

		});
		$.each($('.nav-header'), function(index, val) {
			$(this).nextUntil('li.divider').hide();
			var item = $(this).find('i');			 
			item.attr('class' , 'icon-chevron-left');
		});
		
		$('.alert').alert();
		$('label').click(function(){
			$('input[name=' + $(this).attr('for') + ']').focus();
		});
		$( document ).ajaxStart(function() {
			var box = $("#ajaxLoading") ;
			box.show();
			box.center();
		});
		$( document ).ajaxStop(function(){
			$("#ajaxLoading").hide();
		});
	</script>

	@yield('script')
</body>
</html>