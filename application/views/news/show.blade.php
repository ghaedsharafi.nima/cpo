@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2> {{ $news->subject }} </h2>

   	<?php if(Session::has('msg')) { echo Misc::alert(Session::get('msg'), Session::get('state') ); } ?>

<div>
	<h4><small>توسط: {{ $news->author->name }} در تاریخ {{ Misc::niceDateForge($news->created_at, 'j F y') }}</small></h4>
	<p>{{ $news->content }}</p>
</div>
<div>
<?php $user = Auth::User(); $hasAdminAccess = $user->access == ADMIN_ACCESS; ?>
@foreach($news->comments as $comment)
<div class="well">
	<h3>
		{{ $comment->name }} - <small>{{ Misc::niceDateForge($comment->created_at) }}</small>
		@if($hasAdminAccess and $comment->accepted == true)
    		<a href="{{ URL::to_route('verifyncm', [$comment->id]) }}" class="btn pull-left"><i class="icon-eye-close"></i></a>
    	@elseif($hasAdminAccess and $comment->accepted == false)
	    	<a href="{{ URL::to_route('verifyncm', [$comment->id, !$comment->accepted]) }}" class="btn pull-left"><i class="icon-eye-open"></i></a>
	    @endif
	</h3>
	{{ $comment->comment }}
</div>
@endforeach
</div>
@if(!is_null($user))
<div>
<h3>ارسال نظر</h3>
{{ Form::open(URL::to_route('leavencomment')) }}
{{ Form::hidden('nid', $news->id) }}

{{ Form::text('name', '', ['class' => 'span3', 'placeholder' => 'نام شما']) }}
<div>{{ Form::textarea('comment', '', ['class' => 'span4 makeTextNice' , 'placeholder' => 'متن نظر خود را بنویسید....']) }}</div>

{{ Form::submit('ارسال نظر', ['class' => 'btn']) }}

{{ Form::close() }}
</div>
@endif

@endsection
