@layout('master')

@section('title')
    {{ $title }}
@endsection

@section('content')

<h2>آخرین اخبار</h2>
@foreach($news as $new)
<div>
	<div class="span8 well">
        <h2>
        	{{ $new->subject }}
        	@if(Auth::User()->id == $new->uid)
        		<a href="{{ URL::to_route('deletenews', $new->id) }}" class="btn pull-left" onclick="return confirm('آیا از حذف این خبر اطمینان دارید؟')"><i class="icon-trash"></i></a>
        		<a href="{{ URL::to_route('editnews', $new->id) }}" class="btn pull-left"><i class="icon-edit"></i></a>
        	@endif
        </h2>
        <h4><small>توسط: {{ $new->author->name }} در تاریخ {{ Misc::niceDateForge($new->created_at, 'j F y') }} <i class="icon-comments"></i> {{ $new->comments()->count() }}
        <small></h4>
        <p>{{ Misc::cutWord($new->content, 230)}}</p>
        <h6><a class="pull-left btn" href="{{ URL::to_route('shownews', $new->id) }}">ادامه خبر... </a></h6>
    </div>
</div>
@endforeach
	
@endsection