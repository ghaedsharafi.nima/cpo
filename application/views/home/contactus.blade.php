@layout('master')

@section('title')
    {{ $title }}
@endsection

@section('content')

	{{ Form::open(URL::to_route('contactus')) }}

    <h2> {{ $title }} {{ Form::submit('ارسال', ['class' => 'btn btn-warning']) }}</h2>
	
	<?php if(Session::has('msg')) { echo Misc::alert(Session::get('msg'), Session::get('state') ); } ?>

	{{ Form::text('name', '', ['placeholder' => 'نام و نام خانوادگی شما', 'class' => 'span3']) }}

	{{ Form::text('email', '', ['placeholder' => 'ایمیل شما', 'class' => 'span3']) }}

	<div>
		{{ Form::textarea('message', '', ['placeholder' => 'لطفاً پیام خود را بنویسید...', 'class' => 'makeTextNice span6']) }}
	</div>

	{{ Form::close() }}
@endsection
