@layout('master')
@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2> {{ $title }} </h2>
	<?php if(Session::has('msg')) { echo Misc::alert(Session::get('msg'), Session::get('state') ); } ?>

    {{ Form::open(URL::to_route('settings')) }}

    <label for="name">عنوان پرتال</label>
    {{ Form::text('name', $name, ['class' => 'span4']) }}

    <label for="aboutus">درباره ی ما</label>
    {{ Form::textarea('aboutus', $aboutus, ['class' => 'span4 makeTextNice']) }}

    <div>{{ Form::submit('ثبت', ['class' => 'btn']) }}</div>

    {{ Form::close() }}

@endsection
