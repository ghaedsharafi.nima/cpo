
function ajaxModal(e, modalname, url){
	e.preventDefault();
	if($('#' + modalname).length  == 0)
	{
		$.ajax({
			'type': 'get',
			'dataType': 'html',
			'url' : url,
			'success' : function(result) {
				$('body').append(result);
				$('#' + modalname).modal();
			}
		});
	}
	else
	{
		$('#' + modalname).modal();
	}
}

function newAjaxModal(e, modalname, url){
	e.preventDefault();
	$.ajax({
		'type': 'get',
		'dataType': 'html',
		'url' : url,
		'success' : function(result) {
			$('body').append(result);
			$('#' + modalname).modal();
			$('#' + modalname).on('hidden', function(){
			   	$(this).remove();
			});
		}
	});
}

jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}

window.alert = function(msg) {
	return bootbox.alert(msg);
}